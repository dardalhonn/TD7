#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int main() {
    int i = 0;
    signal(SIGINT, SIG_IGN);
    while (1) {
        printf("\r%d", i);
        fflush(stdout);
        i += 1;
        sleep(1);
    }
    return 0;
}
