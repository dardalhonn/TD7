#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

int cpt_signaux = 0;

int main (){
    int nb_signaux = 100;   // nombre de signaux que le fils va envoyer au père

    int pid = fork();
    if (pid == 0) {
        // envoyer nb_signaux fois le signal SIGUSR1 au processus père

        printf("Fin du processus fils.\n");
    } else {
        // - incrémenter cpt_signaux à chaque fois qu'on reçoit SIGUSR1
        // - afficher la valeur de cpt_signaux et quitter quand on reçoit SIGINT

        while(1) {
            sleep(1);
        };
    }
}
