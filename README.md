# TD7 - Signaux

Cliquez sur le lien ci-dessous pour faire votre fork privé du TP (attention, pas de fork à la main !) :

https://classroom.github.com/a/nJjkvHhz

## Exercice 1. Interception

On considère le programme suivant (`boucle.c`) :
```cpp
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main() {
    int i = 0;
    while (1){
        printf("\r%d", i);  // '\r' revient au début de la ligne sans passer à la ligne suivante
        fflush(stdout);     // cette commande est nécessaire pour afficher les octets en attente de printf
        i += 1;
        sleep(1);
    }
    return 0;
}
```

1. Que fait ce programme ? Compilez-le puis exécutez-le. Comment peut-on l'interrompre (essayez de trouver plusieurs méthodes) fermer l'ide je suppose (grace au gestionnaire de tache au cas ou) moi personnelement j'ai fait controle + C mais il y a d'autre solution.

1. Modifiez le programme pour qu'il affiche un message lorsqu'il reçoit le signal `SIGINT`.
j'ai fait controle+z

1. Comment peut-on envoyer un signal `SIGINT` à un processus (vous connaissez au moins 2 méthodes) ? Lancez le programme et vérifiez son comportement lorsqu'il reçoit le signal. Comment peut-on l'interrompre ?
on peut faire controle+c ou utiliser kill

1. Modifiez maintenant le programme pour qu'il intercepte également les signaux `SIGQUIT`, `SIGTERM`. Testez, et interrompez l'exécution du programme (il reste encore plusieurs possibilités).

1. Que se passe-t-il si l'on essaie d'intercepter le signal `SIGKILL` de la même manière que les autres ?

1. Vérifiez à l'aide de la commande `ps` que tous les processus lancés pendant les questions précédentes ont bien été terminés.


## Exercice 2. Perte de signaux

On va ici observer ce qui se passe lorsqu'un processus envoie très rapidement des signaux à un autre. On considère le code suivant (`perte.c`) :
```cpp
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <unistd.h>

int cpt_signaux = 0;

int main (){
    int nb_signaux = 100;   // nombre de signaux que le fils va envoyer au père

    int pid = fork();
    if (pid == 0) {
        // envoyer nb_signaux fois le signal SIGUSR1 au processus père

        printf("Fin du processus fils.\n");
    } else {
        // - incrémenter cpt_signaux à chaque fois qu'on reçoit SIGUSR1
        // - afficher la valeur de cpt_signaux et quitter quand on reçoit SIGINT

        while(1) {
            sleep(1);
        };
    }
}
```

L'idée ici est de générer un processus à l'aide de `fork()`, puis faire en sorte que le fils envoie un grand nombre de fois (`nb_signaux` fois) le signal `SIGUSR1` au processus parent. De son côté, le parent va incrémenter la variable globale `cpt_signaux` à chaque fois qu'il reçoit un signal `SIGUSR1`.

1. Modifiez le programme pour faire en sorte que le fils envoie `nb_signaux` fois le signal `SIGUSR1` au processus parent. Comment le fils peut-il obtenir le PID du parent ?

1. Modifiez le code du parent pour qu'il incrémente la variable `cpt_signaux` à chaque fois qu'il reçoit le signal `SIGUSR1`.

1. Faites en sorte que le parent affiche la valeur de `cpt_signaux` puis quitte lorsqu'il reçoit un signal `SIGINT`.

1. Testez plusieurs fois votre programme : lancez-le, puis envoyez un signal `SIGINT` pour l'arrêter lorsque le fils a terminé (ça devrait être immédiat). Qu'observez-vous ? Comment expliquez-vous ce comportement ?
